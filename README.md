# awesome-resources
A awesome resources list

## 技术

###iOS

- [Creating your first iOS Framework](https://robots.thoughtbot.com/creating-your-first-ios-framework)<br/>
	这篇文章介绍了如何为一个 Framework 项目添加 `Carthage` 和 `CocoaPods` 支持。

- [How to Create a Framework for iOS](http://www.raywenderlich.com/65964/create-a-framework-for-ios)<br/>
	如果要打包成 `.framework` 的包，并且需要支持 iOS 7，那么这篇文章值得一读。

- [iOS-Framework](https://github.com/jverkoey/iOS-Framework)<br/>
	这个项目在 GitHub 的受关注度也是非常高的。

- [Embedded-Framework-Demo](https://github.com/syshen/Embedded-Framework-Demo)<br/>
	这里介绍了 `Dynamic Framework`，这个是 iOS 8 才开始支持的，里面的 Demo 演示了在 `Objective-C` 项目中如何使用由 `Swift` 语言写的 Library，值得参考一下。

- [如何打造一个让人愉快的框架](https://onevcat.com/2016/01/create-framework/)<br/>
	当然了，[onevcat](https://github.com/onevcat) 的这篇介绍 Framework 的文章，也是不能错过的，中文哦！

- [自学 iOS 开发的一些经验](http://limboy.me/ios/2014/12/31/learning-ios.html)<br/>
	如果你在自学 iOS 开发，那么可以参考下这个。


###Android

**OkHttp**

- [Effective OkHttp](https://github.com/xitu/gold-miner/blob/master/TODO/effective-okhttp.md)

**RxJava**

- [给 Android 开发者的 RxJava 详解](https://gank.io/post/560e15be2dca930e00da1083)
- [用 RxJava 实现事件总线(Event Bus)](http://www.jianshu.com/p/ca090f6e2fe2)



###Git

- [Git 查看、删除、重命名远程分支和 tag](http://zengrong.net/post/1746.htm)
- [Git 常用命令和 Git Flow 梳理](http://jonyfang.com/blog/2015/11/12/git_command_and_git_branching_model/)








